alumnos = [
    {
        nombre: "Nico cass",
        email:"nicolas@gmail.com",
        materia:"FIsica 3"
    },
    {
        nombre: "Coco Caca",
        email:"caca@gmail.com",
        materia:"FIsica 1"
    },
    {
        nombre: "Luck oro",
        email:"luck@gmail.com",
        materia:"Progra 3"
    },
    {
        nombre: "lolo lala",
        email:"lolo@gmail.com",
        materia:"Matematicas 3"
    },
    {
        nombre: "cocq kiko",
        email:"kiko@gmail.com",
        materia:"Recreo 3"
    },

];

const boton = document.querySelector(".btn-confirmar");
const contenedor = document.querySelector(".grid-container");
let htmlCode = "";

for(alumno in alumnos){
    let datos = alumnos[alumno];
    let nombre = datos["nombre"];
    let email = datos["email"];
    let materia = datos["materia"];
    htmlCode += 
    ` <div class="grid-item nombre">${nombre}</div>
        <div class="grid-item email">${email}</div>
        <div class="grid-item materia">${materia}</div>
        <div class="grid-item semana">
            <select class="semana-elegida">
                <option value="Semana 1">Semana 1</option>
                <option value="Semana 2">Semana 2</option>
            </select> 
            </div>`;
           
}

contenedor.innerHTML = htmlCode;

boton.addEventListener("click",()=>{
    let confirmar = confirm("Quiere confirmar las mesas?")
    if(confirmar){
       document.body.removeChild(boton);
        let elementos = document.querySelectorAll(".semana");
        console.log(elementos)
        let semanasElegidas = document.querySelectorAll(".semana-elegida");
        for(elemento in elementos){
            semana = elementos[elemento];
            semana.innerHTML = semanasElegidas[elemento].value;
        
        }
    }

})